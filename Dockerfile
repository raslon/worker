FROM python:3.6.8-stretch

RUN apt-get update  && apt-get -y install locales && apt-get -y install redis-server

RUN adduser --disabled-password --gecos '' worker

ADD requirements.txt /app/requirements.txt

RUN pip3 install --upgrade pip

RUN pip3 install -r /app/requirements.txt

ADD worker /app/worker/

ADD main.py /app/main.py

ADD config.py /app/worker/config.py

RUN chown -R worker:worker /etc/redis/ \
    && chown -R worker:worker /var/log/redis/ \
    && chown -R worker:worker /var/lib/redis/ \
    && chown -R worker:worker /app/

USER worker

WORKDIR /app/

ENTRYPOINT /usr/bin/redis-server /etc/redis/redis.conf && python3 main.py
