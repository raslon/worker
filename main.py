#!/usr/bin/env python3
import asyncio
#import uvloop
from worker.communication import Communication

if __name__ == '__main__':
    #asyncio.set_event_loop_policy(uvloop.EventLoopPolicy())
    loop = asyncio.get_event_loop()
    communication = Communication(loop)
    try:
        loop.run_until_complete(communication.start())
    except KeyboardInterrupt:
        loop.close()


