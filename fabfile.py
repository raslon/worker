from fabric.decorators import hosts
from fabric.api import run


@hosts(
    ['root@46.229.214.49',
     'root@46.229.214.186',
     'root@78.40.219.156',
     'root@89.223.126.117',
     'root@89.223.127.144',
     'root@89.223.127.151',
     'root@89.223.127.163',
     'root@89.223.127.207',
     'root@91.210.168.21',
     'root@91.210.168.59'])
def dep():
    run('docker pull apsavto/worker:aio')
    run('docker-compose down')
    run('docker-compose up -d')


@hosts(
    ['root@46.229.214.49', 'root@46.229.214.186', 'root@78.40.219.156', 'root@89.223.126.117', 'root@89.223.127.144'])
def stop():
    run('docker-compose down')
