from urllib.parse import urlencode
import asyncio
from yarl import URL
import aiohttp
import bs4
import requests
import logging
from python_rucaptcha.ImageCaptcha import aioImageCaptcha, ImageCaptcha
from worker.config import *
from worker.parsers import *


# try:
#     import http.client as http_client
# except ImportError:
#     # Python 2
#     import httplib as http_client
# http_client.HTTPConnection.debuglevel = 1
#
# You must initialize logging, otherwise you'll not see debug output.
# logging.basicConfig()
# logging.getLogger().setLevel(logging.DEBUG)
# requests_log = logging.getLogger('aiohttp.client')
# requests_log.setLevel(logging.DEBUG)
# requests_log.propagate = True


async def testAvito():
    avito = AvitoParser()
    async for d in avito.get_ads(
            search_object={'link': 'https://m.avito.ru/tatarstan/avtomobili?p={page}', 'max_page': 5}):
        print(d)


async def testAM():
    am = AmParser()
    async for d in am.get_ads(
            search_object={'link': 'https://auto.youla.ru/bashkortostan/cars/used/?p={page}', 'max_page': 5}):
        print(d)


async def testDrom():
    drom = DromParser()
    async for d in drom.get_ads(
            search_object={'link': 'https://auto.drom.ru/region16/all/page{page}/', 'max_page': 5}):
        print(d)


async def testIrr():
    irr = IrrParser()
    async for d in irr.get_ads(
            search_object={'link': 'https://tatarstan-resp.irr.ru/cars/passenger/page{page}', 'max_page': 5}):
        continue


async def testAutoSet():
    auto = AutosetParser()
    async for d in auto.get_ads(
            search_object={
                'link': 'https://автосеть.рф/cars/?update=&arrFilter_870=2104355073&set_filter=%D0%9F%D0%BE%D0%BA%D0%B0%D0%B7%D0%B0%D1%82%D1%8C&PAGEN_1={page}',
                'max_page': 5}):
        print(d)

async def testAutoRu():
    auto = AutoParser()
    async for d in auto.get_ads(
            search_object={
                'link': 'https://auto.ru/tatarstan/cars/all/?page={page}',
                'max_page': 10}):
        print(d)

async def testTTS():
    tts = TTSParser()
    async for d in tts.get_ads(
            search_object={
                'link': 'https://www.tts.ru/sprobegom/?PAGEN_1={page}',
                'max_page': 5}):
        print(d)

async def testDrom2():
    dr = DromParser()
    print(await dr.open_and_parse_ad_link('https://naberezhnye-chelny.drom.ru/nissan/x-trail/41191061.html'))

async def testAvito2():
    avito = AvitoParser()
    print(await avito.open_and_parse_ad_link('https://www.avito.ru/nizhnekamsk/avtomobili/kia_rio_x-line_2020_2023637554'))

async def testTTS2():
    tts = TTSParser()
    print(await tts.open_and_parse_ad_link(
        'https://www.tts.ru/sprobegom/detail.php?auto=1263993'))

async def testIrr2():
    irr = IrrParser()
    print(await irr.open_and_parse_ad_link(
        'https://orenburg.irr.ru/cars/passenger/used/vaz-lada-largus-mt-2014-g-advert757307646.html'))

if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    loop.run_until_complete(testDrom2())

# def test3(res=None):
#     if res:
#         text = res.text
#     else:
#         res = requests.get('https://auto.ru/yaroslavskaya_oblast/cars/all/', proxies={'https': 'http://127.0.0.1:8080'},
#                            verify=False)
#         text = res.text
#     bs = bs4.BeautifulSoup(text, features='lxml')
#     if bs.find('form', {'action': '/checkcaptcha'}):
#         print('Auto.ru has captcha {}'.format(EXCHANGE_NAME))
#         key = bs.find('input', {'class': 'form__key'})['value']
#         captcha = bs.find('img', {'class': 'image form__captcha'})['src']
#         ret_path = bs.find('input', {'name': 'retpath'})['value']
#         user_answer = ImageCaptcha(rucaptcha_key=CAPTCHA_KEY, service_type='rucaptcha').captcha_handler(
#             captcha_link=captcha)
#         if not user_answer['error']:
#             res = requests.get('https://auto.ru/checkcaptcha',
#                                params={'rep': user_answer['captchaSolve'], 'key': key, 'retpath': ret_path},
#                                proxies={'https': 'http://127.0.0.1:8080'}, verify=False)
#             test3(res)
#     else:
#         print('ok')
#
#
# async def test1():
#     search_object = {'link': 'https://auto.ru/tulskaya_oblast/cars/all/?page={page}', 'max_page': 5}
#     p = AutoParser()
#     # await p.get('http://127.0.0.1:3000/')
#     async for d in p.get_ads(search_object=search_object):
#         print(d)
#
#
# async def test2(res=None):
#     async with aiohttp.ClientSession(requote_redirect_url=False,
#                                      connector=aiohttp.TCPConnector(limit_per_host=1),
#                                      headers={
#                                          'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36',
#                                          'Accept-Encoding': ', '.join(('gzip', 'deflate')),
#                                          'Accept': '*/*',
#                                          'Connection': 'keep-alive',
#                                      },
#                                      ) \
#             as session:
#         if res:
#             text = await res.text()
#         else:
#             res = await session.get('https://auto.ru/yaroslavskaya_oblast/cars/all/')
#             text = await res.text()
#         bs = bs4.BeautifulSoup(text, features='lxml')
#         if bs.find('form', {'action': '/checkcaptcha'}):
#             print('Auto.ru has captcha {}'.format(EXCHANGE_NAME))
#             key = bs.find('input', {'class': 'form__key'})['value']
#             captcha = bs.find('img', {'class': 'image form__captcha'})['src']
#             ret_path = bs.find('input', {'name': 'retpath'})['value']
#             user_answer = await aioImageCaptcha(rucaptcha_key='ef104be61c36525de6b645d928ff50c6',
#                                                 service_type='rucaptcha').captcha_handler(
#                 captcha_link=captcha)
#             if not user_answer['error']:
#                 url = URL('https://auto.ru/checkcaptcha?%s' %
#                           urlencode({'rep': user_answer['captchaSolve'], 'key': key, 'retpath': ret_path}),
#                           encoded=True)
#                 with (await asyncio.Semaphore(1)):
#                     async with session.get(url) as res:
#                         return await test2(res)
#         else:
#             print('ok')
#
#
# async def test5():
#     p = DromParser()
#     d = await  p.open_and_parse_ad_link(
#         'https://plavsk.drom.ru/great_wall/hover_h3/32873958.html')
#     print(d)
