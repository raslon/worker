import os
REDIS_HOST = os.getenv('REDIS_HOST', 'redis://localhost')

REDIS_PASSWORD = os.getenv('REDIS_PASSWORD')

EXCHANGE_NAME = os.getenv('EXCHANGE_NAME', 'worker#2')

LOG_LEVEL = os.getenv('LOG_LEVEL', 'INFO')

VERBOSE_MODE = os.getenv('VERBOSE_MODE', 'True') == 'True'

AUTO_START = os.getenv('AUTO_START', 'True') == 'True'

DB_HOST = os.getenv('DB_HOST')

DB_LOGIN = os.getenv('DB_LOGIN')

DB_PASSWORD = os.getenv('DB_PASSWORD')

SENTRY_DSN = os.getenv('SENTRY_DSN')

CAPTCHA_KEY = os.getenv('CAPTCHA_KEY')


LOGGING = {
    'version': 1,
    'disable_existing_loggers': True,

    'formatters': {
        'worker': {
            'format': '[%(asctime)s][%(levelname)s] %(message)s',
            'datefmt': '%H:%M:%S',
            },
        },

    'handlers': {
        'console': {
            'level': LOG_LEVEL,
            'class': 'logging.StreamHandler',
            'formatter': 'worker'
            },
        'sentry': {
            'level': LOG_LEVEL,
            'class': 'raven.handlers.logging.SentryHandler',
            'dsn': SENTRY_DSN,
            },
        },

    'loggers': {
        'aiow': {
            'handlers': ['console', 'sentry',],
            'level': LOG_LEVEL,
            'propagate': False,
            },
    }
}