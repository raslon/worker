import json
import asyncio
from asyncio.events import AbstractEventLoop
import time
import logging.config
import aioredis
from aioredis.commands import Redis
from aioredis.pool import ConnectionsPool
import pickle
from typing import AsyncGenerator
import aiojobs
from .config import REDIS_HOST, REDIS_PASSWORD, EXCHANGE_NAME, VERBOSE_MODE, LOGGING, AUTO_START
from .parsers import BaseParser, AutoParser, DromParser, AvitoParser, AmParser, IrrParser, TTSParser, AutosetParser

class Communication(object):

    running_tasks = {}
    sites_dict = \
        {
            BaseParser.SITE_AVITO_RU: AvitoParser,
            BaseParser.SITE_DROM_RU: DromParser,
            BaseParser.SITE_AUTO_RU: AutoParser,
            BaseParser.SITE_IRR_RU: IrrParser,
            BaseParser.SITE_AM_RU: AmParser,
            BaseParser.SITE_TTS_RU: TTSParser,
            BaseParser.SITE_AUTOSET_RF: AutosetParser
        }

    timeouts = \
        {
            BaseParser.SITE_AVITO_RU: 10,
            BaseParser.SITE_AUTO_RU: 5,
            BaseParser.SITE_IRR_RU: 2,
            BaseParser.SITE_DROM_RU: 2,
            BaseParser.SITE_AM_RU: 2,
            BaseParser.SITE_TTS_RU: 2,
            BaseParser.SITE_AUTOSET_RF: 2
        }

    __slots__ = ('loop', 'log', 'sub_redis', 'pub_redis', 'redis_local', 'scheduler',)

    def __init__(self, loop):
        self.loop: AbstractEventLoop = loop
        logging.config.dictConfig(LOGGING)
        self.log = logging.getLogger('aiow.communication')

    async def start(self):
        self.sub_redis:ConnectionsPool = await aioredis.create_pool(address=REDIS_HOST, password=REDIS_PASSWORD, db=4, loop=self.loop)
        self.pub_redis:Redis = await aioredis.create_redis(address=REDIS_HOST, password=REDIS_PASSWORD, db=4, loop=self.loop)
        self.redis_local:Redis = await aioredis.create_redis(address='redis://localhost', loop=self.loop)
        self.scheduler = await aiojobs.create_scheduler()
        await self.pub_redis.pubsub_numsub('data')
        await self.start_consuming()


    async def get_channel(self):
        await self.sub_redis.execute_pubsub('subscribe', EXCHANGE_NAME)
        channel = self.sub_redis.pubsub_channels[EXCHANGE_NAME]
        return channel


    async def publish(self, data):
        data['worker'] = EXCHANGE_NAME
        await self.pub_redis.publish('data', pickle.dumps(data))


    async def send_change(self):
        await self.publish({'type': 'change'})


    async def send_restart(self, site):
        await self.publish({'type': 'start', 'sites': [site]})

    async def produce_data(self, producer: AsyncGenerator, timeout: int):
        async for data in producer:
            if data is not None:
                data.update({'type': 'save'})
                await self.publish(data)
                await asyncio.sleep(timeout)

    async def search(self, search_object):
        site = search_object['site']
        search_class = self.sites_dict[site]
        lock = self.running_tasks.get('worker-{}'.format(site), False)
        if VERBOSE_MODE:
            self.log.info('Will process {} links, site {}'.format(len(search_object['search_objects']), site))
        if not lock:
            full_start = time.time()
            tasks = []
            async with search_class(aioredis=self.redis_local) as producer:
                self.running_tasks['worker-{}'.format(site)] = True
                for object in search_object['search_objects']:

                    object = json.loads(object)
                    if VERBOSE_MODE:
                        self.log.info('Will process {} link, worker {}'.format(object['link'], EXCHANGE_NAME))
                    tasks.append(self.produce_data(producer(search_object=object), self.timeouts[site]))

                await asyncio.gather(*tasks, loop=self.loop)

            self.running_tasks['worker-{}'.format(site)] = False
            full_end = int(time.time() - full_start)
            if full_end > 60:
                self.log.info('Search task for site {} links {} on worker {} finished at {}s'
                          .format(site, len(search_object['search_objects']), EXCHANGE_NAME, full_end))
            await self.send_restart(site)


    async def update(self, links):
        search_objects = {}
        lock = self.running_tasks.get('update', False)
        if not lock:
            self.running_tasks['update'] = True
            start = time.time()
            try:
                for link in links:
                    search_object = search_objects.get(link['advertesiment__site'], None)
                    if not search_object:
                        search_object = self.sites_dict[link['advertesiment__site']]
                        search_objects[link['advertesiment__site']] = search_object
                    data = await search_object().update(link['advertesiment__original_link'])
                    data['pk'] = link['advertesiment__pk']
                    data['type'] = 'update'
                    await self.publish(data)
            finally:
                end = time.time() - start
                self.log.info('Update task finished at {}s'.format(end))
                self.running_tasks['update'] = False

    async def send_autostart_request(self):
        await asyncio.sleep(10)
        data = {'type': 'start', 'sites': ['drom', 'avito', 'irr', 'auto', 'tts', 'autoset']} #later add 'am' and 'auto'
        await self.publish(data)

    async def start_consuming(self):
        self.log.info('Waiting for events. Worker is {}, Verbose Mode: {}, Autostart {}'.format(EXCHANGE_NAME, VERBOSE_MODE, AUTO_START))
        channel = await self.get_channel()
        if AUTO_START:
            await self.scheduler.spawn(self.send_autostart_request())
        while (await channel.wait_message()):
            msg = pickle.loads(await channel.get())
            if msg is not None:
                if msg['type'] == 'start':
                    await self.scheduler.spawn(self.search(search_object=msg))
                elif msg['type'] == 'update':
                    await self.scheduler.spawn(self.update(msg['links']))
            await asyncio.sleep(0.5)
