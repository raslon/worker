import json
import re
from datetime import datetime
import aiohttp
import dateparser
from dateutil import parser
from dateutil.relativedelta import relativedelta
import asyncio
from random import randint
from .base import BaseParser, Constants

class IrrParser(BaseParser):
    SITE_NAME = 'irr.ru'
    params_dict = \
        {
            'Год выпуска': 'date',
            'Пробег': 'mileage',
            'Тип кузова': 'body',
            'Цвет': 'color',
            'Тип двигателя': 'motor',
            'Тип трансмиссии': 'transmission',
            'Марка': 'mark',
            'Модель': 'model',
            'Контактное лицо': 'author',
            'Привод': 'drive_type',
            'Мощность двигателя': 'motor_power',
            'Объем двигателя': 'motor_volume',
            'Состояние автомобиля': 'state',
            'Руль': 'helm'
        }

    headers = {'Connection': 'keep-alive',
               'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36'}
    carInfo_re = re.compile(r'product\[\'listingParams\'\]\ \=\ (.+)\;', re.MULTILINE)
    motor_data_re = re.compile(r'(\d.\d)\sл\s/\s(\d+)\sл.с.\s/\s([а-я]+)')

    time_irr_re = re.compile(r'(\d+:\d+)')

    date_irr_re = re.compile(r'(\d+\s[а-я]+)')



    link_selector = {'class': 'listing__itemTitle'}

    # async def get(self, url, **kwargs):
    #     async with aiohttp.ClientSession(headers=self.headers) as session:
    #         res = await session.get(url, **kwargs)
    #         return await res.text()

    async def open_and_parse_ad_link(self, link):
        data = {}
        data.update({'original_link': link})
        data.update({'site': Constants.SITE_IRR})
        await asyncio.sleep(randint(5, 12))
        text = await self.get(link)
        bs = self.bs4(text)


        # if 'new' in link:
        #     state = Constants.STATE_NEW
        # else:
        #     state = Constants.STATE_WITH_MILEAGE
        # data.update({'state': state})
        raw_car_info = self.carInfo_re.search(str(bs.find('script', text=self.carInfo_re)))[1]

        car_info = json.loads(raw_car_info)
        #
        # data['mark'] = car_info['make']
        # data['model'] = car_info['model']
        #
        # if 'volume' is car_info:
        #     data['motor_volume'] = float(car_info['volume'])
        # else:
        #     data['motor_volume'] = 'Не указан'
        # data['transmission'] = car_info['transmittion']
        # if 'engine-power' in car_info:
        #     data['motor_power'] = int(car_info['engine-power'])
        # else:
        #     data['motor_power'] = 'Не указан'
        # data['body'] = car_info['bodytype']
        #
        # if 'turbo' is car_info:
        #     data['motor'] = car_info['turbo']
        # else:
        #     data['motor'] = 'Не указан'
        #
        # if 'gear' is car_info:
        #     drive_type = car_info['gear']
        #     if drive_type == 'постоянный полный':
        #         data['drive_type'] = 'полный'
        #     else:
        #         data['drive_type'] = car_info['gear']
        # else:
        #     data['drive_type'] = 'Не указан'
        #
        # if 'color' is car_info:
        #     data['color'] = car_info['color']
        # else:
        #     data['color'] = 'Не указан'
        if 'car-year' in car_info:
            data['date'] = datetime(year=int(car_info['car-year']), month=1, day=1)
        # data['mileage'] = car_info['mileage']
        # data['price'] = car_info['price']
        # data['city'] = car_info['address_city']
        # data['region'] = car_info['address_region']
        data['created_on_site'] = parser.parse(car_info['date_create'])
        #
        # if bs.find('i', {'class': 'icon_house'}) is not None:
        #     seller = Constants.SELLER_COMPANY
        # elif bs.find('i', {'class': 'icon_head'}) is not None:
        #     seller = Constants.SELLER_PRIVATE_PERSON
        # else:
        #     seller = Constants.SITE_NOT_SELECTED
        # data.update({'seller': seller})
        #
        # photo = bs.find('div', {'class': 'lineGallery js-lineProductGallery'})
        # if photo is not None:
        #     data.update({'photo_link': photo.find('img')['data-src']})
        #
        # print(data)
        # return data

        # Новый/С пробегом
        if 'new' in link:
            state = Constants.STATE_NEW
        else:
            state = Constants.STATE_WITH_MILEAGE
        data.update({'state': state})

        # Дата добавления
        created_on_site = bs.find('div', {'class': 'productPage__createDate'})
        if created_on_site is not None:
            created_on_site = created_on_site.find('span').text.strip()
            match = self.time_irr_re.search(created_on_site)
            if match:
                date_time = match.group(1)
                if 'сегодня' in created_on_site:
                    date = parser.parse(date_time)
                    data.update({'created_on_site': date})
                elif 'вчера' in created_on_site:
                    date = parser.parse(date_time) - relativedelta(days=1)
                    data.update({'created_on_site': date})
                else:
                    data.update({'created_on_site': datetime.now()})
            else:
                match = self.date_irr_re.match(created_on_site)
                if match:
                    date = dateparser.parse(match.group(1))
                    data.update({'created_on_site': date})
                else:
                    self.set_bad(data['original_link'])
        else:
            self.set_bad(data['original_link'])

        # Тип продавца
        if bs.find('i', {'class': 'icon_house'}) is not None:
            seller = Constants.SELLER_COMPANY
        elif bs.find('i', {'class': 'icon_head'}) is not None:
            seller = Constants.SELLER_PRIVATE_PERSON
        else:
            seller = Constants.SITE_NOT_SELECTED
        data.update({'seller': seller})

        # Фотография
        photo = bs.find('div', {'class': 'lineGallery js-lineProductGallery'})
        if photo is not None:
            data.update({'photo_link': photo.find('img')['data-src']})

        # Срочно?
        description = bs.find('p', {'class': 'productPage__descriptionText'})
        if description is not None:
            data['quickly'] = 'срочно' in description.text.lower()

        # Цена
        price = bs.find('div', {'class': 'productPage__price'})
        if price is not None:
            match = self.price_re.search(price.text.strip())
            if match is not None:
                price = float(''.join(match.group(1).split()))
                data.update({'price': price})
            else:
                data.update({'price': 0})
        else:
            data.update({'price': 0})

        # Цвет
        color = bs.find('li', {'class': 'productPage__productColor'})
        if color is not None:
            data.update({'color': color.text.strip()})

        # Город, у irr нет возможности получить регион или GeoPoint, что очень хренова
        city = bs.find('div', {'class': 'js-scrollToMap'})
        if city is not None:
            data.update({'city': city.text.strip().split(',')[0]})
        else:
            city = bs.find('a', {'class': 'js-selectRegionButton'})
            if city:
                data.update({'city': city.text.strip()})
            else:
                self.set_bad(data['original_link'])

        # Заголовки - по заголовку определяем как блок мы щас обрабатываем.
        titles = bs.find_all('h3', {'class': 'productPage__infoColumnBlockTitle'})
        for title in titles:
            params = title.parent.find_all('li', {'class': 'productPage__infoColumnBlockText'})
            title = title.text.strip()
            # Из общей информации нам нужна только марка, модель и автор
            # Из Дополнительной  - привод
            if title in ['Общая информация', 'Дополнительная информация']:
                for param in params:
                    param = param.text.strip()
                    if ':' in param:
                        param_name, param_value, *_ = param.split(':')
                        param_value = param_value.strip()
                        param_name = self.params_dict.get(param_name)
                        if param_name is not None:
                            if param_name == 'mileage':
                                # Пробег парсим по регулярке
                                match = self.int_re.match(param_value)
                                if match is not None:
                                    param_value = int(match.group(1))
                                else:
                                    param_value = 0
                            elif param_name == 'date':
                                # Год выпуска
                                match = re.search(self.int_re, param_value)
                                if match is not None:
                                    param_value = datetime(year=int(match.group(1)), month=1, day=1)
                            elif param_name == 'motor_volume':
                                # Объём двигателя
                                match = self.float_re.search(param_value)
                                if match is not None:
                                    param_value = float(match.group(1))
                                else:
                                    match = self.int_re.search(param_value)
                                    param_value = float(match.group(1))
                            elif param_name == 'motor_power':
                                # Мощность двигателя
                                match = self.int_re.search(param_value)
                                if match is not None:
                                    param_value = int(match.group(1))

                            elif param_name == 'state':
                                param_value = Constants.STATE_WITH_MILEAGE if 'б/у' in param_value else Constants.STATE_NEW
                            data.update({param_name: param_value})
        date = bs.find()
        if data.get('helm') is None:
            data['helm'] = 'Не указан'
        if data.get('date') is None:
            data['date'] = datetime.now()
        if data.get('body') is None:
            data['body'] = 'Не указан'
        return data


    async def update(self, ad_link):
        text = await self.get(ad_link)
        bs = self.bs4(text)
        data = {'is_deleted': False}
        price = bs.find('div', {'class': 'productPage__price'})
        if price is not None:
            match = self.price_re.search(price.text.strip())
            if match is not None:
                price = float(''.join(match.group(1).split()))
                data.update({'price': price})
            else:
                data.update({'price': 0})
        else:
            data['is_deleted'] = True

        return data