import re
import urllib.parse
from datetime import datetime
import json
import asyncio
from .base import BaseParser, Constants
from random import randint


class AvitoParser(BaseParser):
    SITE_NAME = 'avito.ru'

    pattern = re.compile(r'window\.__initialData__ \= \"(.+)\"', re.MULTILINE)
    # pattern_2 = re.compile(r'window\.__initialData__\ =', re.MULTILINE)

    headers = {'Connection': 'keep-alive',
               'User-Agent': 'Mozilla/5.0 (Linux; Android 7.0; SM-G892A Build/NRD90M; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/67.0.3396.87 Mobile Safari/537.36'}
    user_agent = 'Mozilla/5.0 (Linux; Android 7.0; SM-G892A Build/NRD90M; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/67.0.3396.87 Mobile Safari/537.36'

    params_dict = {
        'Год выпуска': 'date',
        'Тип двигателя': 'motor',
        'Пробег, км': 'mileage',
        'Тип кузова': 'body',
        'Цвет': 'color',
        'Объём двигателя': 'motor_volume',
        'Коробка передач': 'transmission',
        'Привод': 'drive_type',
        'Руль': 'helm',
        'Состояние': 'beaten',
        'Тип автомобиля': 'state',
        'Мощность двигателя, л.с.': 'motor_power',
        'Владельцев по ПТС': 'owners_count'
    }

    mark_model_re = re.compile(r'^([А-яA-z\-]+)\s(.+)\s(\d\.\d)\+?\xa0(\w{2,3}),\s(\d{4}),\s([А-я]+)')

    link_selector = {'data-marker': 'item/link'}

    base_url = 'https://m.avito.ru{0}'

    async def open_and_parse_ad_link(self, link):

        await asyncio.sleep(randint(5,12))
        res = await self.get(link)
        bs_raw = self.bs4(res)
        raw_text = str(bs_raw.find('script', text=self.pattern))

        data = {'site': Constants.SITE_AVITO}
        item_raw_data = None
        try:
            item_raw_data = urllib.parse.unquote(raw_text.split('"')[1])

        except Exception:
            await asyncio.sleep(randint(5, 12))
            return await self.open_and_parse_ad_link(link)

        if item_raw_data:
            # raw_data = item_raw_data.text.split('window.__initialData__ = ')[-1][:]
            # raw_data = re.split('\ \|\|', raw_data)[0]
            raw_data = json.loads(item_raw_data)

            if raw_data['item']:
                try:
                    raw_data = raw_data['item']['item']
                    # Ссылка на объявление
                    data['original_link'] = raw_data['sharing']['url']
                    data['site'] = Constants.SITE_AVITO

                    data['geo_point'] = (raw_data['coords']['lng'], raw_data['coords']['lat'])
                    data['created_on_site'] = datetime.utcfromtimestamp(raw_data['time'])

                except Exception:
                    print()

                # for param in raw_data['parameters']['flat']:
                #     param_name = self.params_dict.get(param['title'], None)
                #     if param_name:
                #         data.update({param_name: param['description']})

                if data.get('state', '') == 'С пробегом':
                    data['state'] = Constants.STATE_WITH_MILEAGE
                else:
                    data['state'] = Constants.STATE_NEW

                data['price'] = float(raw_data['price']['value'].replace(' ', ''))
                # data['verified'] = True#raw_data['autoteka']['reportAvailable']
                if raw_data['userType'] == 'company':
                    data['seller'] = Constants.SELLER_COMPANY
                else:
                    data['seller'] = Constants.SELLER_PRIVATE_PERSON

                data['author'] = raw_data['seller']['name'].split('\n')[0]
                groups = (raw_data['firebaseParams'])
                if groups:
                    if groups['vehicle_type'] == 'С пробегом':
                        data['state'] = Constants.STATE_WITH_MILEAGE
                    else:
                        data['state'] = Constants.STATE_NEW

                    if 'condition' in groups:
                        if groups['condition'] == 'Битый':
                            data['beaten'] = True
                        else:
                            data['beaten'] = False
                    else:
                        data['beaten'] = False

                    if 'mileage' in groups:
                        data['mileage'] = int(re.sub('[ км]', '', groups['mileage']))
                    else:
                        data['mileage'] = 0
                    data['motor'] = groups['engine_type']
                    data['drive_type'] = groups['drive']
                    data['color'] = groups['color']
                    data['mark'] = groups['brand']
                    data['model'] = groups['model']
                    data['transmission'] = groups['transmission']
                    if 'engine' in groups:
                        data['motor_volume'] = float(groups['engine'])
                    data['date'] = datetime(year=int(groups['year']), day=1, month=1)
                    data['body'] = groups['body_type']
                    data['motor_power'] = int(groups['capacity'].split(' л.с.')[0])
                    data['helm'] = groups['wheel']

                    if 'vladeltsev_po_pts' in groups:
                        if groups['vladeltsev_po_pts'] == '4+':
                            data['owners_count'] = 4
                        else:
                            data['owners_count'] = groups['vladeltsev_po_pts']
                    else:
                        data['owners_count'] = 0
                    if raw_data.get('images', None):
                        data['photo_link'] = raw_data['images'][0]['640x480']
                    else:
                        data['photo_link'] = None
                    return data

                else:
                    await self.set_bad(link)
            else:
                await self.set_bad(link)

    #
    # async def update(self, link):
    #     text = await self.get(url=link.replace('www.', 'm.'))
    #     data = {'is_deleted': False}
    #     bs = self.bs4(text)
    #     item_raw_data = bs.find('script', text=self.pattern)
    #     if item_raw_data:
    #         raw_data = item_raw_data.text.split('window.__initialData__ = ')[-1][:-6]
    #         raw_data = json.loads(raw_data)['item']['item']
    #         data['price'] = float(raw_data['price']['value'].replace(' ', ''))
    #         return data
