import re
import time
import json
from urllib.parse import unquote
from datetime import datetime
from datetime import timedelta
from .base import BaseParser, Constants
from ..config import EXCHANGE_NAME, VERBOSE_MODE


# Всё получение контента идёт через web-driver
class AmParser(BaseParser):
    SITE_NAME = 'am.ru'
    params_dict = {
        'Год выпуска': 'date',
        'Пробег': 'mileage',
        'Кузов': 'body',
        'КПП': 'transmission',
        'Двигатель': 'motor',  # тип и объём идут вместе
        'Руль': 'helm',
        'Цвет': 'color',
        'Привод': 'drive_type',
        'Мощность': 'motor_power',
        'Владельцев': 'owners_count'
    }

    headers = {'Connection': 'keep-alive',
               'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36'}

    state_re = re.compile(r'window\.transitState \= decodeURIComponent\("(.+)"\);', re.MULTILINE)

    img_re = re.compile(r'background-image: url\("(.+)"\);')

    motor_re = re.compile(r'([А-я]+)(?=\s/\s(\d+\.?\d{0,2})\sл)')

    mileage_re = re.compile(r'(\d{0,3})\s?(\d+)\sкм')

    minute_ago_re = re.compile(r'Обновлено\s(\d{1,2})\sминут')

    hour_ago_re = re.compile(r'Обновлено\s(\d{1,2})\sчас')

    days_ago_re = re.compile(r'Обновлено\s(\d{1,2})\sд')

    link_selector = {'data-target': 'serp-snippet-title'}

    async def open_and_parse_ad_link(self, ad_link):
        data = {}
        data['original_link'] = ad_link
        data['site'] = Constants.SITE_AM
        text = await self.get(ad_link)
        bs = self.bs4(text)
        raw_state = bs.find('script', text=self.state_re)
        state = json.loads(unquote((self.state_re.search(str(raw_state)).group(1))))
        # print(state[1][17][1])
        geopoint = {}
        enu = enumerate(state[1][17][1])
        for pos, text in enu:
            if text == 'lat':
                geopoint['lng'] = next(enu)[1]
            elif text == 'lng':
                geopoint['lat'] = next(enu)[1]

            if text == 'contacts':
                contact = enumerate(next(enu)[1][1])
                # for c_pos, c_text in contact:
                #     if c_text == 'city':
                #         data['city'] = (next(contact)[1])

                seller = enumerate(next(enu)[1][1])
                for s_pos, s_text in seller:

                    if s_text == 'page':
                        data['seller'] = Constants.SELLER_COMPANY
                    else:
                        data['seller'] = Constants.SELLER_PRIVATE_PERSON
                    if s_text == 'seller':
                        data['author'] = next(seller)[1]
        data['geo_point'] = (float(geopoint['lat']), float(geopoint['lng']))

        data['state'] = Constants.STATE_WITH_MILEAGE
        # марка
        mark = bs.find('div', {'data-target': 'item-breadcrumbs-brand'}).find('a', {'class', 'blueLink'}).text.strip()
        data['mark'] = mark
        # модель
        model = bs.find('div', {'data-target': 'item-breadcrumbs-model'}).find('a', {'class', 'blueLink'}).text.strip()
        data['model'] = model.replace('\n', '')
        # цена
        price = bs.find('div', {'data-target': 'advert-price'}).text.strip()
        data['price'] = int(price.replace('\u2009', ''))
        # фото
        photo_link_raw = None
        index = 0
        while photo_link_raw is None:
            photo_link_raw = bs.find('figure', {'data-index': str(index)})
            index += 1

        photo_link = photo_link_raw.find('img')['src']
        data['photo_link'] = photo_link

        # год выпуска
        year_ = bs.find('div', {'data-target': 'advert-info-year'}).find('a').text.strip()
        date = datetime(year=int(year_), day=1, month=1)
        data['date'] = date
        # Пробег
        mileage = (bs.find('div', {'data-target': 'advert-info-mileage'}))
        if mileage:
            if mileage.text.strip().replace(' ', '').replace('км', '') == 'Неуказан':
                data['mileage'] = 0
            else:
                data['mileage'] = mileage.text.strip().replace(' ', '').replace('км', '')
        else:
            data['mileage'] = 0
        # Кузов
        body = bs.find('div', {'data-target': 'advert-info-bodyType'}).text.strip().split(' ')[0]
        data['body'] = body
        # КПП
        transmission = bs.find('div', {'data-target': 'advert-info-transmission'}).text.strip()
        data['transmission'] = transmission
        # Тип двигателя
        motor = bs.find('div', {'data-target': 'advert-info-engineInfo'}).text.strip().split(' / ')[0]
        data['motor'] = motor
        # Объем двигателя
        motor_volume = \
            bs.find('div', {'data-target': 'advert-info-engineInfo'}).text.strip().split(' / ')[1].split(' ')[0]
        data['motor_volume'] = motor_volume
        # Руль
        helm = bs.find('div', {'data-target': 'advert-info-wheelType'}).text.strip()
        data['helm'] = helm
        # Цвет
        color = bs.find('div', {'data-target': 'advert-info-color'}).text.strip()
        data['color'] = color
        # Привод
        drive_type = bs.find('div', {'data-target': 'advert-info-driveType'}).text.strip()
        data['drive_type'] = drive_type
        # Мощность
        motor_power = bs.find('div', {'data-target': 'advert-info-enginePower'}).text.strip().split(' ')[0]
        data['motor_power'] = motor_power
        # Владельцев
        owners_count = bs.find('div', {'data-target': 'advert-info-owners'})
        if owners_count:
            data['owners_count'] = owners_count.text.strip()
        else:
            data['owners_count'] = 0
        # Дата обновления
        data['created_on_site'] = datetime.now()
        return data

    #     raw_data = json.loads(unquote(self.state_re.search(state.text).group(1)))
    #     # Ебических размеров костыль, но они ТАК хранят у себя state
    #     try:
    #         geopoint = {}
    #         enu = enumerate(raw_data[1][15][1])
    #         for pos, text in enu:
    #             if text =='lat':
    #                 geopoint['lat'] = next(enu)[1]
    #             elif text == 'lng':
    #                 geopoint['lng'] = next(enu)[1]
    #             elif text == 'contacts':
    #                 seller = enumerate(next(enu)[1][1])
    #                 for s_pos, s_text in seller:
    #                     if s_text == 'page':
    #                         if 'salon' in next(seller)[1]:
    #                             data['seller'] = Constants.SELLER_COMPANY
    #                         else:
    #                             data['seller'] = Constants.SELLER_PRIVATE_PERSON
    #                     elif s_text == 'seller':
    #                         data['author'] = next(seller)[1]
    #         data['geo_point'] = (float(geopoint['lng']), float(geopoint['lat']))
    #     except Exception as e:
    #         self.log.error('[Am.ru (PARSE)] error {} while state handling, {}'.format(e, ad_link))
    #         return
    #     # # Марка, модель
    #     try:
    #         data['mark'] =  bs.find('div', {'data-target-id': 'item-breadcrumbs-brand'}).text.strip()
    #         model = bs.find('div', {'data-target-id': 'item-breadcrumbs-pseudomodel'})
    #         if model is None:
    #             model = bs.find('div', {'data-target-id': 'item-breadcrumbs-model'})
    #         data['model'] = model.text.strip()
    #     except:
    #         self.log.warning('[Am.ru (PARSE)] Mark or Model not found, link {0}'.format(data['original_link']))
    #         return
    #
    #     # Фото
    #     photo = bs.find('div', {'class': 'PhotoGallery_photoWrapper__3m7yM'})
    #     if photo is not None:
    #         photo = photo.find('button').get('style', None)
    #         if photo is not None:
    #             match = self.img_re.match(photo)
    #             if match is not None:
    #                 data['photo_link'] = match.group(1)
    #     # Цена
    #     price = bs.find('div', {'data-target' :'advert-price'})
    #     if price is not None:
    #         price = price.text.strip()
    #         match = self.price_re.search(price)
    #         if match is not None:
    #             data['price'] = int(match.group(1).encode('ascii', 'ignore').decode('utf-8'))
    #         else:
    #             data['price'] = 0
    #     else:
    #         self.log.warning('[Am.ru (PARSE)] Price not found, link {0}'.format(data['original_link']))
    #         data['price'] = 0
    #
    #     # Основные параметры автомобиля
    #     params = bs.find_all('div', {'class': 'AdvertSpecs_row__ljPcX'})
    #     for _param in params:
    #         if _param is not None:
    #             param = _param.find('div', {'class': 'AdvertSpecs_label__2JHnS'}).text.strip()
    #
    #             # Если есть VIN то ставим что проверен
    #             if param == 'VIN':
    #                 data['verified'] = True
    #                 continue
    #             value = _param.find('div', {'class': 'AdvertSpecs_data__xK2Qx'}).text.strip()
    #             param = self.params_dict.get(param, None)
    #             # Данные по двигателю идут парой тип-объём
    #             if param == 'motor':
    #                 match = self.motor_re.search(value)
    #                 if match is not None:
    #                     data['motor'] = match.group(1)
    #                     data['motor_volume'] = float(match.group(2))
    #                 else:
    #                     data['motor'] = value
    #                     data['motor_volume'] = 0.0
    #             else:
    #                 if param is not None:
    #                     # Год автомобиля
    #                     if param == 'date':
    #                         value = datetime(year=int(value), day=1, month=1)
    #                     # Пробег
    #                     if param == 'mileage':
    #                         match = self.mileage_re.search(value)
    #                         if match is not None:
    #                             value = int('{}{}'.format(match.group(1), match.group(2)))
    #                             data['state'] = Constants.STATE_WITH_MILEAGE
    #                         else:
    #                             value = 0
    #                             data['state'] = Constants.STATE_NEW
    #                     # Кол-во владельцев
    #                     if param == 'owners_count':
    #                         match = self.int_re.match(value)
    #                         if match is not None:
    #                             value = int(match.group(1))
    #                     # Мощность двигателя
    #                     if param == 'motor_power':
    #                         match = self.int_re.search(value)
    #                         if match is not None:
    #                             value = int(match.group(1))
    #                         else:
    #                             value = 0
    #                     data.update({param: value})
    #
    #     # Выставить дату создания, хотя тут скорее "дата обновления"
    #     date_update = bs.find('span', {'class': 'AdvertCard_statActualizationDate__d2Msz'})
    #     if date_update is not None:
    #         date_update = date_update.text.strip()
    #         match = self.minute_ago_re.search(date_update)
    #         if match is not None:
    #             delta = timedelta(minutes=int(match.group(1)))
    #         else:
    #             match = self.hour_ago_re.search(date_update)
    #             if match is not None:
    #                 delta = timedelta(hours=int(match.group(1)))
    #             else:
    #                 match = self.days_ago_re.search(date_update)
    #                 if match is not None:
    #                     delta = timedelta(days=int(match.group(1)))
    #                 else:
    #                     delta = timedelta(seconds=1)
    #         data['created_on_site'] = datetime.now() - delta
    #     else:
    #         data['created_on_site'] = datetime.now()
    #     # Если состояния нет - то помечаем новым
    #     if data.get('state') is None:
    #         data['state'] = Constants.STATE_NEW
    #     return data
    #
    # async def update(self, ad_link):
    #     text = await self.get(ad_link)
    #     data = {'is_deleted': False}
    #     bs = self.bs4(text)
    #     price = bs.find('div', {'data-target': 'advert-price'})
    #     if price is not None:
    #         price = price.text.strip()
    #         match = self.price_re.search(price)
    #         if match is not None:
    #             data['price'] = int(match.group(1).replace(' ', ''))
    #         else:
    #             data['price'] = 0
    #     else:
    #         self.log.warning('[Am.ru (PARSE)] Price not found, link {0}'.format(data['original_link']))
    #         data['is_deleted'] = True
    #
    #     return data
