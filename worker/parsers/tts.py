import asyncio
from datetime import datetime
import re
from random import randint

from .base import BaseParser, Constants


class TTSParser(BaseParser):
    SITE_NAME = 'tts.ru'

    headers = {'Connection': 'keep-alive',
               'User-Agent': 'Mozilla/5.0 (Linux; Android 7.0; SM-G892A Build/NRD90M; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/67.0.3396.87 Mobile Safari/537.36'}
    user_agent = 'Mozilla/5.0 (Linux; Android 7.0; SM-G892A Build/NRD90M; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/67.0.3396.87 Mobile Safari/537.36'
    vol_re = re.compile(r'(\d.\d)')
    base_url = ''
    # tts_selector = r'(bx_\d{9}_\d{5-8})' #bx_565502798_1166427
    vol_re = re.compile(r'(\d.\d)')
    motor_power_re = re.compile(r'\d{2-3}')

    base_url = 'https://www.tts.ru{0}'

    link_selector = {'class': 'preview-card_auto used-auto'}
    params_dict = \
        {
            'Марка:': 'mark',
            'Модель:': 'model',
            'Тип кузова:': 'body',
            'Объём двигателя, л:': 'motor_volume',
            'Тип трансмиссии:': 'transmission',
            'Год выпуска:': 'date',
            'Мощность в лошадиных силах:': 'motor_power',
            'Пробег:': 'mileage',
            'Тип двигателя:': 'motor',
            'Привод:': 'drive_type',
            'Цвет:': 'color'
        }

    async def open_and_parse_ad_link(self, link):
        if 'auction' not in link:
            await asyncio.sleep(randint(2, 5))
            data = {}
            text = await self.get(link)
            bs = self.bs4(text)
            #try:
            data.update({'original_link': link})
            data.update({'site': Constants.SITE_TTS})
            city = bs.find('div', {'class': 'card-auto_address--item'}).text.strip()
            data.update({'city': city.split(',')[0]})
            img = bs.find('img', {'class', 'img'})['src']
            data.update({'photo_link': img})
            data.update({'helm': 'Не указан'})
            data.update({'author': 'ТТС'})
            data.update({'seller': Constants.SELLER_COMPANY})
            data.update({'created_on_site': datetime.now()})
            data.update({'state': Constants.STATE_WITH_MILEAGE})
            price = bs.find('div', {'class': 'now-price-text'}).text.strip().split(' q')[0].replace(' ', '')
            data.update({'price': int(price)})

            state_card = bs.find('div', {'class': 'tooltip-wrapper auto-card_icons'})
            if state_card:
                state = state_card.find('span', {'class': 'tooltip_text'}).text.strip().split()[0]

                if state == 'Один':
                    data.update({'owners_count': 1})
                if state == 'Два':
                    data.update({'owners_count': 2})
                if state == 'Три':
                    data.update({'owners_count': 3})
                if state == 'Четыре':
                    data.update({'owners_count': 4})
            else:
                data.update({'owners_count': 1})


            table_body = bs.find('tbody', {'class': 'table-body'})
            for table in table_body.find_all('tr'):
                key = self.params_dict[table.find('td', {'class': 'single-characteristic__title'}).text.strip()]
                value = table.find('td', {'class': 'single-characteristic__value'}).text.strip()
                data.update({key: value})
                if key == 'motor_volume':
                    data.update({key: float(value)})
                if key == 'body':
                    data.update({key: value.split()[0]})
                if key == 'color':
                    data.update({key: value.split()[0]})
                if key == 'motor_power':
                    data.update({key: int(value)})
                if key == 'mileage':
                    data.update({key: int(value.split(' км')[0].replace(' ', ''))})
                if key == 'motor':
                    if value.split()[0] == 'электрический':
                        data.update({key: 'Электро'})
                    else:
                        data.update({key: value.split()[0]})
                if key == 'date':
                    data.update({key: datetime(year=int(value), day=1, month=1)})



            return data

        # data.update({'created_on_site': created_on_site,
        #              'state': Constants.STATE_WITH_MILEAGE,
        #              'city': city,
        #              'mileage': int(mileage),
        #              'body': body.split(' ')[0],
        #              'motor': engine_type.split(' ')[0],
        #              'drive_type': drive_type,
        #              'color': color.split(' ')[0],
        #              'price': price,
        #              'verified': True,
        #              'seller': Constants.SELLER_COMPANY,
        #              'author': 'ТТС',
        #              'mark': brand,
        #              'model': model,
        #              'owners_count': owners_count,
        #              'transmission': kpp,
        #              'motor_volume': float(re.search(self.vol_re, motor_volume).group(0)),
        #              'date': datetime(year=int(year), month=1, day=1),
        #              'motor_power': int(engine_power.split(' л.с.')[0]),
        #              'photo_link': bs.find('ul', {'class': 'photosBig'}).find('li')['data-src'],
        #              'helm': 'Не указан'
        #              })
        #except:
            #await self.set_bad(link)