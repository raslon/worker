import re
from datetime import datetime
import json
from .base import BaseParser, Constants
import asyncio
from random import randint
import dateparser
from dateutil.relativedelta import relativedelta

from ..config import EXCHANGE_NAME, VERBOSE_MODE

class AvitoParser(BaseParser):
    SITE_NAME = 'avito.ru'

    pattern = re.compile(r'window\.dataLayer\ =\ \[(.+)\]', re.MULTILINE)

    user_agent = 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36'

    time_avito_re = re.compile(r'(\d+:\d+)')

    time_avito_re2 = re.compile(r'(\d{2}\d{2})')

    date_avito_re = re.compile(r'(\d+\s[А-я]+)')

    params_dict = {
        'Год выпуска': 'year',
        'Тип двигателя': 'motor',
        'Пробег, км': 'mileage',
        'Тип кузова': 'body',
        'Цвет': 'color',
        'Объём двигателя': 'engine',
        'Коробка передач': 'transmission',
        'Привод': 'drive',
        'Руль': 'wheel',
        'Состояние': 'condition',
        'Тип автомобиля': 'body_type',
        'Мощность двигателя, л.с.': 'capacity',
        'Владельцев по ПТС': 'vladeltsev_po_pts'
    }

    mark_model_re = re.compile(r'^([А-яA-z\-]+)\s(.+)\s(\d\.\d)\+?\xa0(\w{2,3}),\s(\d{4}),\s([А-я]+)')

    link_selector = {'class': 'snippet-link'}

    base_url = 'https://www.avito.ru{0}'


    async def open_and_parse_ad_link(self, link):
        await asyncio.sleep(randint(5, 12))
        res = await self.get(link)
        bs = self.bs4(res)
        data = {'site': Constants.SITE_AVITO}
        data['original_link'] = link.replace('m.','www')
        city = bs.find('span', {'class': 'item-address__string'}).text.split(', ')
        data['city'] = city[1]
        item_raw_data = bs.find('script', text=self.pattern)
        if item_raw_data:

            raw_data_all = item_raw_data.text.split('window.dataLayer = ')[-1][:-397]
            raw_data = json.loads(raw_data_all)[1]


            data['mark'] = raw_data['brand']
            data['model'] = raw_data['model']
            data['price'] = raw_data['itemPrice']
            data['body'] = raw_data['body_type']


            # Дата размещения
            if bs.find('div', {'class': 'title-info-metadata-item-redesign'}):
                date = bs.find('div', {'class': 'title-info-metadata-item-redesign'}).text.strip()
                year = self.int_re.search((bs.find('span', {'itemprop': 'name'})).text)
                match = self.time_avito_re.search(date)
                match2 = self.time_avito_re2.search(date)
                if match or match2:
                    date_time = match.group(1) if match is not None else match2.group(1)
                    if 'сегодня' in date:
                        date = dateparser.parse(date_time)
                    elif 'вчера' in date:
                        date = dateparser.parse(date_time) - relativedelta(days=1)
                    else:
                        date_search = self.date_avito_re.search(date)
                        if match is not None or date_search:
                            date = dateparser.parse(date_search.group(1) + ' ' + match.group(1))
                        else:
                            date = dateparser.parse(datetime.now())
                else:
                    date = dateparser.parse(datetime.now())
                data['created_on_site'] = date

                author_first_type = bs.find('div', {'class': 'seller-info-col'})
                author_second_type = bs.find('div', {'class': 'seller-info-prop'})
                author = author_second_type if author_first_type is None else author_first_type
                if author:
                    if 'Автодилер' in author.text or 'Магазин' in author.text:
                        shop_name = author.find('div', {'class': 'seller-info-name'})
                        profile = shop_name.find('a')
                        if profile:
                            shop_name = profile.text.strip()
                        else:
                            shop_name = shop_name.text.strip()
                        if shop_name:
                            data['seller'] = Constants.SELLER_COMPANY
                    else:
                        data['seller'] = Constants.SELLER_PRIVATE_PERSON
                # if raw_data['type_of_trade']=='Продаю личный автомобиль':
                #     data['seller'] = Constants.SELLER_PRIVATE_PERSON
                # else:
                #     data['seller'] = Constants.SELLER_COMPANY

                for param in self.params_dict.values():
                    param_name = raw_data.get(param, None)
                    data[param] = param_name
                data['motor'] = raw_data['engine_type']
                data['motor_power'] = raw_data['capacity']
                data['transmission'] = raw_data['transmission']
                data['driver_type'] = raw_data['drive']
                data['color'] = raw_data['color']
                data['helm'] = raw_data['wheel']
                data['body'] = raw_data['body_type']
                data['motor_volume'] = raw_data['engine']




                if 'mileage' in raw_data:
                    data['mileage'] = raw_data['mileage']
                    data['mileage'] = int(data['mileage'].replace(' ', ''))
                else:
                    data['mileage'] = '0'
                if 'condition' in raw_data:
                    if raw_data['condition'] == 'Не битый':
                        data['beaten'] = False
                    else:
                        data['beaten'] = True
                else:
                    data['beaten'] = False
                if raw_data['vehicle_type'] == 'С пробегом':
                    data['state'] = Constants.STATE_WITH_MILEAGE
                else:
                    data['state'] = Constants.STATE_NEW
                if raw_data['year']:
                    data.update({'date': datetime(year=int(raw_data['year']), month=1, day=1)})
                else:
                    data.update({'date': datetime.now()})
                if data.get('mileage'):
                    data['mileage'] = int(data['mileage'].replace(' ', '').replace('км', ''))
                else:
                    data['mileage'] = 0
                if data.get('motor_power'):
                    data['motor_power'] = int(data['motor_power'].replace(' ', '').replace('л.с.', ''))
                else:
                    data['motor_power'] = 0

            image = 'http:' + bs.find('div', {'class': 'gallery-img-frame'}).find('img').get('src')
            if image:
                data['photo_link'] = image
            else:
                data['photo_link'] = None
        else:
            await self.set_bad(link)
        return data



    async def update(self, link):
        text = await self.get(url=link.replace('www.', 'www.'))
        data = {'is_deleted': False}
        bs = self.bs4(text)
        item_raw_data = bs.find('script', text=self.pattern)
        if item_raw_data:
            raw_data = item_raw_data.text.split('window.dataLayer = ')[-1][:-397]
            raw_data = json.loads(raw_data)[1]
            data['price'] = raw_data['itemPrice']
            return data
