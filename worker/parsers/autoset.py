import asyncio
from datetime import datetime
import re
from random import randint

import dateparser

from .base import BaseParser, Constants


class AutosetParser(BaseParser):
    SITE_NAME = 'автосеть.рф'
    city_pattern = re.compile(r'г.(\s[А-я]+)', re.MULTILINE)

    headers = {'Connection': 'keep-alive',
               'User-Agent': 'Mozilla/5.0 (Linux; Android 7.0; SM-G892A Build/NRD90M; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/67.0.3396.87 Mobile Safari/537.36'}
    user_agent = 'Mozilla/5.0 (Linux; Android 7.0; SM-G892A Build/NRD90M; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/67.0.3396.87 Mobile Safari/537.36'

    base_url = 'https://автосеть.рф{}'
    params_dict = {
        'Марка': 'mark',
        'Модель': 'model',
        'Год выпуска': 'date',
        'Пробег,\xa0км\n': 'mileage',
        'Тип кузова': 'body',
        'Цвет': 'color',
        'Объем,\xa0куб.см\n': 'motor_volume',
        'Тип двигателя': 'motor',
        'Мощность,\xa0л.с.\n': 'motor_power',
        'Тип КПП': 'transmission',
        'Тип привода': 'drive_type',
        'Руль': 'helm'
    }

    async def open_and_parse_ad_link(self, link):
        await asyncio.sleep(randint(2, 3))
        data = {}
        data.update({'original_link': link})
        data.update({'site': Constants.SITE_AUTOSET})
        text = await self.get(link)
        bs = self.bs4(text)
        props = bs.find_all('div', {'class': 'prop'})
        if props:

            data['created_on_site'] = created_on_site = datetime.now()
            # data['geo_point'] = eval(bs.find('a', {'class': 'icon-location-map'})['data-point'])
            data['city'] = \
            (re.search(self.city_pattern, bs.find('div', {'class': 'col-xs-12 col-md-8 address'}).text).group(0)).split(
                'г. ')[1]

            data['state'] = 2
            data['seller'] = 2
            data['beaten'] = False
            data['owners_count'] = 1
            data['verified'] = True
            data['price'] = (bs.find('span', {'class': 'priceNum'}).text).replace("'", "")
            data['author'] = 'Автосеть.рф'
            try:
                data['photo_link'] = self.base_url.format(bs.find('a', {'class': 'fstImg'})['href'])
            except:
                pass
            if 'photo_link' not in data:
                data['photo_link'] = 'https://автосеть.рф/bitrix/templates/newstyle/images/nophoto.jpg'
            for prop in props:
                data_ = (prop.find('div', {'class': 'data'})).find('span').text
                value_ = (prop.find('div', {'class': 'value'})).find('span').text

                if self.params_dict[data_.split('  ')[0]]:
                    data[self.params_dict[data_.split('  ')[0]]] = value_
            data['date'] = dateparser.parse(data['date'])
            data['motor_volume'] = round(float(data['motor_volume']) / 1000, 1)
            data['motor_power'] = int(data['motor_power'])
            return data
        # else:
        #     await self.set_bad(link)
