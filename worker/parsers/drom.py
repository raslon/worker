import json
import re
import dateparser
import asyncio
from datetime import datetime
from .base import BaseParser, Constants
from random import randint


class DromParser(BaseParser):
    SITE_NAME = 'drom.ru'
    params_dict = \
        {
            'Двигатель': 'motor',
            'Мощность': 'motor_power',
            'Трансмиссия': 'transmission',
            'Привод': 'drive_type',
            'Цвет': 'color',
            'Пробег, км': 'mileage',
            'Пробег': 'mileage',
            'Руль': 'helm',
            'Тип кузова': 'body',
            'Пробег по России': 'mileage_on_russia',
            'Дополнительно': 'quickly',
            'Особые отметки': 'beaten',
            'Тип кузова': 'body'

        }
    car_data = re.compile(r"data\-custom\-redirect\-params\=\'(.+)\'", re.MULTILINE)
    headers = {'Connection': 'keep-alive',
               'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36'}

    motor_drom_re = re.compile(r'([а-я]+),\s(\d.\d) л')

    created_on_site_drom_re = re.compile(r'Объявление \d+ от (\d{2})-(\d{2})-(\d{4})')

    date_drom_re = re.compile(r'(\d+)\sгод')

    link_selector = {'data-ftid': 'bulls-list_bull'}

    async def open_and_parse_ad_link(self, link):
        # await asyncio.sleep(randint(1,2))
        data = {}
        data.update({'original_link': link})
        # У drom.ru нету опций
        data.update({'site': Constants.SITE_DROM})
        text = await self.get(link)

        bs = self.bs4(text)

        # Город-Регион

        reg = re.compile(r'Город: ')
        elements_div = [e for e in bs.find_all('div') if reg.match(e.text)]
        city_text = ''
        city = ''
        try:
            city_text = elements_div[0].text.split(',')
            city = city_text[0].replace('Город: ', '')
        except Exception:
            # Город может не появиться, если объявление свежо
            meta_city = bs.find('meta',{'property':'og:description'}).attrs['content'].split(',')[2]
            city = meta_city
        text_footer = bs.find('div', class_='b-text b-text_align_center b-text_size_s b-text_color_gray-dark').text
        region = re.findall(r"\((\w+.+)\)", text_footer)[0]

        data['city'] = city.replace(',', '')
        data['region'] = region

        # сверка параметров обЪявления со словарём

        table_body_tr = bs.find('table').find_all('tr')
        th = [tr.find('th').text for tr in table_body_tr if tr.find('th') and tr.find('td').text != '']
        td = [tr.find('td').text for tr in table_body_tr if tr.find('td') and tr.find('td').text != '']
        th_td = dict(zip(th, td))
        for params in self.params_dict.keys():
            if params in th_td:
                value = th_td[params]
                data[self.params_dict[params]] = value

        motor_text = None
        try:
            motor_text = data['motor'].split(',')
            if 'л' in motor_text[0]:
                raise Exception
            data['motor'] = motor_text[0].replace(',', '')
        except Exception:
            data['motor'] = 'Двигатель не указан'  # попалось обьявление, где нет инфы даже о типе двигателя

        try:
            data['motor_volume'] = float(motor_text[1].replace(' л', ''))

        except Exception:
            print(link)
            if motor_text is not None:
                if 'л' in motor_text[0] and motor_text[0] != 'дизель' and motor_text[0] != 'электро':
                    data['motor_volume'] = float(motor_text[0].replace(' л', ''))
                else:
                    data['motor_volume'] = 0
            else:
                data['motor_volume'] = 0


        try:
            data['motor_power'] = int(data['motor_power'].replace('\xa0л.с.', ''))
        except Exception:
            data['motor_power'] = 0  # и такое случилось

        # ГБО меняем на бензин
        if data['motor'] =='ГБО':
            data['motor'] = 'Бензин'
        # # Битый?
        if 'mileage' in data:
            if 'новый автомобиль' or 'новый от неофициального дилера' in data['mileage']:
                data['state'] = Constants.STATE_NEW
                data['beaten'] = False
                data['mileage'] = 0
            else:
                try:
                    data['state'] = Constants.STATE_WITH_MILEAGE
                    if ',' in data['mileage']:
                        data['mileage'] = int(data['mileage'].split(',')[0].replace(',', '').replace(' ', ''))
                    else:
                        data['mileage'] = int(data['mileage'].replace(' ', ''))
                    if 'beaten' in data:
                        data['beaten'] = 'требуется ремонт или не на ходу' in data['beaten']
                    else:
                        data['beaten'] = False
                except Exception as e:
                    print(e)
                    pass
        else:
            data['state'] = Constants.STATE_NEW
            data['beaten'] = False
            data['mileage'] = 0

        # марка, модель, год выпуска
        script = json.loads(bs.find_all('script', {'type': 'application/ld+json'})[0].contents[0])

        data['mark'] = script['brand']

        if data['mark'] == 'Прочие авто':
            return await self.set_bad(link)

        data['model'] = script['name'].replace(script['brand'] + ' ', '')

        info_block = bs.find('h1', text=re.compile("Продажа.*"))
        info_block_text = info_block.text.split(',')

        year = int(info_block_text[1].strip().split(' ')[0])
        data['date'] = data['date'] = datetime(year=int(year), day=1, month=1)

        # Дата создания на сайте
        reg = re.compile(r'Объявление ')
        creatation_div_text = [e for e in bs.find_all('div') if reg.match(e.text)][1].text.strip()
        date = creatation_div_text.split(' ')[3].split('.')
        data.update({'created_on_site': datetime(year=int(date[2]), month=int(date[1]),
                                                 day=int(date[0]))})

        # фото
        photo = bs.find_all('img')
        if photo:
            try:
                photo_link = photo[0].attrs['src']
                data['photo_link'] = photo_link
            except Exception as e:
                data['photo_link'] = None

        # цена

        info_tag = bs.find('meta', {'name': 'candy.config'})
        into_json = json.loads(info_tag.attrs['content'])

        data['price'] = float(into_json['cf']['p'])

        # карточка продавца
        prodavan_span = [i for i in bs.find_all('span') if (len(i.contents)) > 0 and (
                    i.contents[0] == 'Посмотреть карточку продавца' or i.contents[0] == 'Карточка продавца:')]

        if len(prodavan_span) > 0:
            data.update({'seller': Constants.SELLER_COMPANY})
        else:
            data.update({'seller': Constants.SELLER_PRIVATE_PERSON})

        transissions = dict(Constants.TRANSMISSION_SEARCH)

        try:
            if 'transmission' in data:
                data['transmission'] =data['transmission'].capitalize()
            else:
                data['transmission'] = 'Механика'

            # if data['motor'] != 0:
            #     data['motor'] = \
            #         list(filter(lambda x: x[1] == data['motor'].capitalize(), Constants.MOTOR))[0][0]
        except Exception as e:
            data['transmission'] = Constants.TRANSMISSION_NOT_SELECTED

        if 'drive_type' not in data:
            data['drive_type'] = 'Передний'

        # Кузов может быть не указан
        if data.get('body') is None:
            data['body'] = 'Не указан'
        else:
            if data['body'] == 'джип/suv 5 дв.':
                data['body'] = 'Внедорожник'
            if data['body'] == 'хэтчбек 5 дв.':
                data['body'] = 'Хэтчбек'

        # Пробег может быть не указан
        if data.get('mileage') is None:
            data['mileage'] = 0

        if 'color' not in data:
            data['color'] = 'Цвет не указан'

        if data.get('helm') is None:
            data['helm'] = 'Не указан'

        ap = data['helm'].title()
        data['author']='-'


        return data

    async def update(self, ad_link):
        text = await self.get(ad_link)
        bs = self.bs4(text)
        data = {'is_deleted': False}
        price = bs.find('div', {'class': ' css-fvvv4o e144fp8x0'})

        if price is not None:
            match = self.price_re.search(price.text.strip())
            if match is not None:
                data.update({'price': int(''.join(match.group(1).split()))})
            else:
                data.update({'price': 0})
        else:
            data['is_deleted'] = True
        return data

    #     try:
    #         data = {}
    #         data.update({'original_link': link})
    #         # У drom.ru нету опций
    #         data.update({'site': Constants.SITE_DROM})
    #         text = await self.get(link)
    #
    #         bs = self.bs4(text)
    #         # Город-Регион
    #         city_region = bs.find('span', text='Город:')
    #         city_region = city_region.next_sibling.split(',')
    #         if len(city_region) > 1:
    #             data['city'] = city_region[0].strip()
    #             data['region'] = city_region[1].strip()
    #         else:
    #             data['city'] = city_region[0].strip()
    #         if data.get('city') is None:
    #             self.set_bad(link)
    #         # Марка, модель
    #         mark, model = bs.find_all('div', {'class': 'b-breadcrumbs__item'})[2:]
    #         data.update({'mark': mark.text.strip()})
    #         data.update({'model' :model.text.strip()})
    #
    #         # Цена
    #         price = bs.find('div', {'class': 'css-fvvv4o e144fp8x0'})
    #         if price is None:
    #             price = bs.find('div', {'class': 'css-6oq18s e144fp8x0'})
    #         if price is not None:
    #             match = self.price_re.search(price.text.strip())
    #             if match is not None:
    #                 data.update({'price': int(''.join(match.group(1).split()))})
    #             else:
    #                 data.update({'price': 0})
    #         else:
    #             data.update({'price': 0})
    #
    #         # Дата создания на сайте
    #         media_cont = bs.find_all('div', {'class': 'b-media-cont'})
    #         for el in media_cont:
    #             if 'Объявление' in el.text:
    #                 match = self.created_on_site_drom_re.match(el.text.strip())
    #                 if match is not None:
    #                     data.update({'created_on_site': datetime(year=int(match.group(3)), month=int(match.group(2)), day=int(match.group(1)))})
    #
    #         # Год автомобиля
    #
    #         for date in bs.find_all('h1', {'class': 'b-title b-title_type_h1 b-title_no-margin b-text-inline'}):
    #             match = self.date_drom_re.search(date.text.strip())
    #             if match is not None:
    #                 data.update({'date': datetime(year=int(match.group(1)), month=1, day=1)})
    #         if data.get('date', None) is None:
    #             data.update({'date': datetime.now()})
    #
    #
    #
    #         # Проверен?
    #         data['verified'] = bs.find('div', {'class': 'b-tip b-tip_theme_doc'}) is not None
    #
    #         # Фотография
    #         photo = bs.find('a', {'class': 'b-advItemGallery__photo'})
    #         if photo:
    #             data['photo_link'] = photo['href']
    #
    #
    #         # Основные данные автомобиля
    #         params = bs.find('div', {'class': 'b-media-cont_margin_b-size-s b-media-cont_relative'}).find_all('span', {'class': 'b-text-gray'})
    #         for param in params:
    #             param_name = self.params_dict.get(param.text.strip())
    #             if param_name is not None:
    #                 param_value = param.find_next_siblings(text=True)[0].strip()
    #                 # Данные по двигателю идут связкой, тип и объём.
    #                 if param_name == 'motor':
    #                     match = self.motor_drom_re.search(param_value)
    #                     if match is not None:
    #                         data.update({'motor': match.group(1)})
    #                         data.update({'motor_volume': float(match.group(2))})
    #                     else:
    #                         match = self.float_re.match(param_value)
    #                         if match:
    #                             data.update({'motor_volume': float(match.group(1))})
    #                         else:
    #                             data.update({'motor_volume': 0.0})
    #                             # Мощьность хранится в отдельном элементе
    #                 else:
    #                     if param_name == 'motor_power':
    #                         param_value = bs.find('div', {'class': 'b-link b-link_dashed'}).text.strip()
    #                         match = self.int_re.search(param_value)
    #                         if match is not None:
    #                             param_value = int(match.group(1))
    #                         else:
    #                             self.set_bad(link)
    #                     elif param_name == 'mileage':
    #                         # Проверим новый или с пробегом
    #                         match =  self.int_re.search(param_value)
    #                         # Если сматчился пробег - то он с пробегом
    #                         if match is not None:
    #                             param_value = match.group(1)
    #                             data.update({'state': Constants.STATE_WITH_MILEAGE})
    #                         else:
    #                             param_value = 0
    #                             data.update({'state': Constants.STATE_NEW})
    #                     # Может не быть числового пробега, но указан параметр "Пробег по России"
    #                     elif param_name == 'mileage_on_russia':
    #                         if param_value == 'есть':
    #                             data.update({'state': Constants.STATE_WITH_MILEAGE})
    #                         else:
    #                             data.update({'state': Constants.STATE_NEW})
    #                     # Если в комментарии есть слово "срочно" - помечаем как срочный, такой способ кране не надёжен, но его предложил PM
    #                     elif param_name == 'quickly':
    #                         param_value = 'срочно' in param_value
    #                     data.update({param_name: param_value})
    #
    #         # Костыли основанные на наличии текста, но у drom.ru херовая вёрстка, так что вот так
    #         if 'Посмотреть карточку продавца' in text or 'Карточка продавца:' in text:
    #             data.update({'seller': Constants.SELLER_COMPANY})
    #         else:
    #             data.update({'seller': Constants.SELLER_PRIVATE_PERSON})
    #
    #         # Битый?
    #         data['beaten'] = 'требуется ремонт или не на ходу' in text
    #
    #         # Кузов может быть не указан кузов
    #         if data.get('body') is None:
    #             data['body'] = 'Не указан'
    #
    #         # Пробег может быть не указан
    #         if data.get('mileage') is None:
    #             data['mileage'] = 0
    #
    #         if data.get('color') is None:
    #             data['color'] = 'Цвет не указан'
    #
    #         if data.get('helm') is None:
    #             data['helm'] = 'Не указан'
    #         return data
    #     except:
    #         await self.set_bad(link)
    #
    # async def update(self, ad_link):
    #     text = await self.get(ad_link)
    #     bs = self.bs4(text)
    #     data = {'is_deleted': False}
    #     price = bs.find('div', {'class': ' css-fvvv4o e144fp8x0'})
    #
    #     if price is not None:
    #         match = self.price_re.search(price.text.strip())
    #         if match is not None:
    #             data.update({'price': int(''.join(match.group(1).split()))})
    #         else:
    #             data.update({'price': 0})
    #     else:
    #         data['is_deleted'] = True
    #
    #     return data
