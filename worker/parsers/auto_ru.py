import re
import json
from urllib.parse import urlencode
from yarl import URL
from json.decoder import JSONDecodeError
import dateparser
import requests
from requests import Response
from requests.cookies import cookiejar_from_dict
import asyncio
from python_rucaptcha.ImageCaptcha import aioImageCaptcha
from .base import BaseParser, Constants
from ..config import EXCHANGE_NAME, CAPTCHA_KEY
from random import randint
from datetime import datetime


# Всё получение контента идёт через web-driver
class AutoParser(BaseParser):
    SITE_NAME = 'auto.ru'
    params_dict = {
        'Кузов': 'body',
        'Цвет': 'color',
        'Двигатель': 'motor',  # Двигатель у auto.ru содержит в себе объём, мощность и тип, разбор по регулярке.
        'Коробка': 'transmission',
        'Привод': 'drive_type',
        'Руль': 'helm',
    }
    headers = {'Connection': 'keep-alive',
               'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36'}

    motor_volume = re.compile(r'(\d{1}\.\d{1})')

    auto_electro_motor_details_re = re.compile(r'(\d+)\sл.с.\s/\s(\d+)\s[А-я]+\s/\s([А-я]+)')

    transmission_re = re.compile('[A-Z]{2,}')

    body_re = re.compile('[а-я]{3,}')

    create_data = re.compile('Дата\ размещения\ объявления')

    cookies = {'gdpr': '1', 'X-Vertis-DC': 'sas'}

    link_selector = {'class': 'ListingItemTitle-module__link'}

    # def __init__(self):
    #     super().__init__()
    #     self.session = requests.Session()
    #     self.session.headers.update(self.headers)
    #     self.session.cookies = cookiejar_from_dict(self.cookies)
    #
    # async def get(self, url, **kwargs)-> Response:
    #     res = await asyncio.get_event_loop().run_in_executor(None, self.session.get, url)
    #     res.encoding = res.apparent_encoding
    #     return res

    async def open_and_parse_ad_link(self, ad_link):
        await asyncio.sleep(randint(2, 5))

        text = await self.get(ad_link)
        # text.encoding = 'utf-8'
        bs = self.bs4(text)
        state = bs.find('script', {'id': 'initial-state'})

        ad = json.loads(state.contents[0])
        if ad['card']['seller']['location'].get('coord', None):
            data = {'site': Constants.SITE_AUTO}
            data['original_link'] = ad_link
            data['geo_point'] = (ad['card']['seller']['location']['coord']['longitude'],
                                 ad['card']['seller']['location']['coord']['latitude'])
            data['author'] = str(ad['card']['seller']['name'])
            data['mark'] = str(ad['card']['vehicle_info']['mark_info']['name'])
            data['model'] = str(ad['card']['vehicle_info']['model_info']['name'])
            data['photo_link'] = 'https:%s' % ad['card']['state']['image_urls'][0]['sizes']['1200x900']
            data['beaten'] = not ad['card']['state'].get('state_not_beaten')
            data['price'] = ad['card']['price_info'].get('RUR', 0.0)
            data['mileage'] = ad['card']['state']['mileage']
            data['owners_count'] = ad['card']['documents'].get('owners_number', 0)
            # data[''] = ad['card']['state'].get('mileage', 0
            data['motor_power'] = ad['card']['vehicle_info']['tech_param'].get('power', 0)
            info = str(ad['card']['lk_summary']).split(', ')
            data['body'] = info[1].split(' ')[0]
            data['body'] = re.search(self.body_re, info[1]).group(0)
            data['transmission'] = (re.search(self.transmission_re, info[0])).group(0)
            data['motor_volume'] = (re.search(self.motor_volume, info[0])).group(0)
            data['motor'] = info[3]
            data['drive_type'] = info[2]
            # Костыли для "обязательных" параметров которых может не оказаться в объявлении.
            if not data.get('mileage'):
                data.update({'mileage': 0})

            if not data.get('transmission'):
                data.update({'transmission': 'Не указана'})
            if data['mileage'] != 0:
                data['state'] = Constants.STATE_WITH_MILEAGE
            else:
                data['state'] = Constants.STATE_NEW
            data['date'] = dateparser.parse('%s' % ad['card']['documents']['year'])

            if ad['card']['vehicle_info']['steering_wheel'] == 'LEFT':
                data['helm'] = 'Левый'
            else:
                data['helm'] = 'Правый'
            color = bs.find('li', {'class': 'CardInfoRow CardInfoRow_color'})
            if color:
                data['color'] = (color.find_all('span', {'class': 'CardInfoRow__cell'})[1].text.strip())
            if 'color' not in data:
                color = bs.find('li', {'class': 'CardInfoGrouped__row CardInfoGrouped__row_color'})
                colordiv = color.find('div', {'class': 'CardInfoGrouped__cellLabel'})
                data['color'] = colordiv.find('a', {'class': 'Link CardInfoGrouped__cellValue'}).text.strip()

            cart = bs.find_all('div', {'title': self.create_data})
            if cart:
                for cart_info in cart:
                    if cart_info.get('title') is not None and 'Дата' in cart_info['title']:
                        data['created_on_site'] = dateparser.parse(cart_info.text.strip())
            else:
                data['created_on_site'] = datetime(year=datetime.now().year,
                                                   month=datetime.now().month,
                                                   day=datetime.now().day)

            if bs.find('a', {'class': 'Link Link_color_black CardSellerNamePlace__name_dealer'}) is not None:
                data['seller'] = Constants.SELLER_COMPANY
            # elif bs.find('svg',{'class':'IconSvg IconSvg_official-dealer IconSvg_size_48'}) is not None:
            #     data['seller'] = Constants.SELLER_COMPANY
            else:
                data['seller'] = Constants.SELLER_PRIVATE_PERSON
            return data

    async def update(self, ad_link):
        res = await self.get(ad_link)
        bs = self.bs4(res)
        state = bs.find('script', {'id': 'initial-state'})
        data = {'is_deleted': False}
        if state is None or state.text is None:
            data['is_deleted'] = True
            return data
        try:
            ad = json.loads(state.text)
            data['price'] = ad['card']['price_info']['RUR']
        except JSONDecodeError:
            self.log.error('Auto.ru JSON Decode ERROR')
            data['is_deleted'] = True

        return data

    async def check_captcha(self, res: Response, task_id=None):
        bs = self.bs4(res)
        if bs.find('form', {'action': '/checkcaptcha'}):
            if task_id:
                await self.report_bad(task_id)
            self.log.warning('Auto.ru has captcha {}'.format(EXCHANGE_NAME))
            key = bs.find('input', {'class': 'form__key'})['value']
            captcha = bs.find('img', {'class': 'image form__captcha'})['src']
            ret_path = bs.find('input', {'name': 'retpath'})['value']
            user_answer = await aioImageCaptcha(rucaptcha_key=CAPTCHA_KEY, service_type='rucaptcha').captcha_handler(
                captcha_link=captcha)
            if not user_answer['error']:
                url = URL('https://auto.ru/checkcaptcha?%s' %
                          urlencode({'rep': user_answer['captchaSolve'], 'key': key, 'retpath': ret_path}),
                          encoded=True)
                res = await self.get(url)
                return await self.check_captcha(res, user_answer['taskId'])
            else:
                self.log.error('RuCaptcha error {}'.format(str(user_answer['errorBody']['text'])))
                return await self.check_captcha(await self.get(res.url))
        else:
            return bs
