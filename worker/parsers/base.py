import re
import traceback
from asyncio import TimeoutError
import asyncio
import logging
from typing import AsyncGenerator
from functools import partial
import aiohttp
from aiohttp.client_exceptions import ClientConnectionError, ServerDisconnectedError
from abc import ABCMeta, abstractmethod
import bs4
from python_rucaptcha.RuCaptchaControl import aioRuCaptchaControl
import aiopg
from aioredis.commands import Redis
from worker.config import DB_HOST, DB_LOGIN, DB_PASSWORD, CAPTCHA_KEY, LOG_LEVEL, EXCHANGE_NAME, VERBOSE_MODE
from random import randint

class Constants(object):
    """
    Объявление
    """
    DRIVE_NOT_SELECTED = 5
    DRIVE = (
        ('', 'Любой привод'),
        (1, 'Передний'),
        (2, 'Задний'),
        (3, 'Полный'),
        (4, '4WD'),  # drom.ru что это за привод я хз
    )

    TRANSMISSION_NOT_SELECTED = 5
    TRANSMISSION_SEARCH = (
        ('', 'Коробка передач'),
        (1, 'Механика'),
        (1, 'Механическая'),
        (2, 'Автомат'),
        (2, 'Автоматическая'),
        (3, 'Робот'),
        (4, 'Вариатор'),
    )

    TRANSMISSION = (
        ('', 'Любая коробка'),
        (1, 'MT'),
        (2, 'AT'),
        (2, 'AMT'),
        (3, 'Робот'),
        (4, 'Вариатор'),
    )
    MOTOR_NOT_SELECTED = 6
    MOTOR = (
        ('', 'Все типы двигателя'),
        (1, 'Бензин'),
        (2, 'Газ'),
        (3, 'Дизель'),
        (4, 'Электро'),
        (5, 'Гибрид'),
    )

    STATE_NEW = 1
    STATE_WITH_MILEAGE = 2
    STATE = (
        ('', 'С пробегом и без'),
        (STATE_NEW, 'Новый'),
        (STATE_WITH_MILEAGE, 'С пробегом'),
    )

    SELLER_PRIVATE_PERSON = 1
    SELLER_COMPANY = 2
    SELLER_NOT_SELECTED = 3
    SELLER = (
        ('', 'Продавец'),
        (SELLER_PRIVATE_PERSON, 'Частное лицо'),
        (SELLER_COMPANY, 'Компания'),
    )
    HELM_NOT_SELECTED = 4
    HELM_TYPE = (
        ('', 'Руль любой'),
        (1, 'Правый'),
        (2, 'Левый'),
        (3, 'Не указан'),
    )

    SITE_NOT_SELECTED = 1
    SITE_AVITO = 2
    SITE_AUTO = 3
    SITE_IRR = 4
    SITE_DROM = 5
    SITE_AM = 6
    SITE_TTS = 7
    SITE_AUTOSET = 8
    SITE = (
        (SITE_NOT_SELECTED, 'Не указан'),
        (SITE_AVITO, 'avito.ru'),
        (SITE_AUTO, 'auto.ru'),
        (SITE_IRR, 'irr.ru'),
        (SITE_DROM, 'drom.ru'),
        (SITE_AM, 'am.ru'),
        (SITE_TTS, 'tts.ru'),
        (SITE_AUTOSET, 'автосеть.рф'),
    )


# Базовый класс парсера
class BaseParser(object, metaclass=ABCMeta):
    SITE_NAME = None
    # Константы сайтов
    SITE_AVITO_RU = 'avito'

    SITE_AUTO_RU = 'auto'

    SITE_IRR_RU = 'irr'

    SITE_DROM_RU = 'drom'

    SITE_AM_RU = 'am'

    SITE_TTS_RU = 'tts'

    SITE_AUTOSET_RF = 'autoset'

    cookies: dict = {}

    bs4 = partial(bs4.BeautifulSoup, features='lxml')

    int_re = re.compile(r'(\d+)')

    float_re = re.compile(r'(\d+\.\d+)')

    price_re = re.compile(r'(\d{0,3}\s?\d+\s\d+)')

    link_selector: dict = None

    base_url: str = None

    headers: dict = None
    motor_data_re = re.compile(r'(\d.\d)\sл\s/\s(\d+)\sл.с.\s/\s([а-я]+)')

    __slots__ = ('log', 'session', 'aioredis', 'search_object', 'current_page',)

    def __init__(self, aioredis=None):
        self.log = logging.getLogger('aiow.parsers')
        self.log.setLevel(LOG_LEVEL)
        self.session = aiohttp.ClientSession(headers=self.headers,
                                              cookies=self.cookies,
                                              requote_redirect_url=False,)
        if aioredis:
            self.aioredis:Redis = aioredis

    async def get_ads(self, search_object=None) -> AsyncGenerator:

        if search_object:
            self.search_object = search_object
            self.current_page = int(self.search_object['max_page'])
        while self.current_page >= 1:
            link = self.search_object['link'].format(page=self.current_page)
            if VERBOSE_MODE:
                self.log.info('{} page {}, {}'.format(self.SITE_NAME, link, EXCHANGE_NAME))

            text = await self.get(link)



            bs = self.bs4(text)

            if self.SITE_AVITO_RU or self.SITE_AUTO_RU:
                await asyncio.sleep(randint(3, 6))

            if self.SITE_NAME == 'автосеть.рф':
                bs = bs.find('div', {'class': 'row catalogTbl'})

            for link in bs.find_all('a', self.link_selector):
                if self.base_url:
                    ad_link = self.base_url.format(link['href'])
                else:
                    ad_link = link['href']
                if await self.exists(ad_link) != 1 and not await self.is_bad(ad_link):
                    yield await self.open_and_parse_ad_link(ad_link)
            self.current_page -= 1

    @abstractmethod
    async def open_and_parse_ad_link(self, link: str) -> dict:
        """
        Открывает и парсит страницу объявления
        :param link:
        :return: dict
        """
        pass

    async def get(self, url, **kwargs)-> str:
        """
        Используется для загрузки контента в рамках текущей сессии, сохраняя куки.
        :param url:
        :param kwargs:
        :return:
        """
        res = await self.session.get(url, max_redirects=200, **kwargs)

        return await res.content.read()

    async def exists(self, link: str) -> int:
        """
        Проверяет наличие объявления в базе главного сайта
        :param link:
        :return:
        """
        if self.SITE_NAME == 'avito.ru':
            link = link.replace('m.', 'www.')
        conn = None
        try:
            conn = await  aiopg.connect(
                "dbname='indexavto' user='%s' host='%s' password='%s'" % (DB_LOGIN, DB_HOST, DB_PASSWORD))
            async with conn.cursor() as cur:
                await cur.execute("SELECT COUNT(*) FROM cars_advertesiment WHERE original_link = '%s'" % link)
                r = await cur.fetchone()
                return int(r[0])
        except Exception as e:
            self.log.error('DB ERORR {}'.format(str(e)))
            return -1
        finally:
            if conn:
                await conn.close()

    async def report_bad(self, wrong_captcha_id: str) -> None:
        await aioRuCaptchaControl(rucaptcha_key=CAPTCHA_KEY).additional_methods(action='reportbad',
                                                                                        id=wrong_captcha_id)

    async def check_captcha(self, res, task_id=None):
        return self.bs4(res)

    async def set_bad(self, link):
        if self.aioredis:
            await self.aioredis.set(link, 1, expire=24*60*60)

    async def is_bad(self, link):
        if self.aioredis:
            return await self.aioredis.exists(link) > 0
        else:
            return False

    async def __aenter__(self):
        return self.get_ads

    async def __aexit__(self, exc_type, exc_val, exc_tb):
        try:
            await self.session.close()
        except:
            pass
        is_ignore = isinstance(exc_val, (TimeoutError, ClientConnectionError, ServerDisconnectedError,))
        if exc_tb and not is_ignore:
            self.log.error('Site {} has error {} {} traceback:\n{}'.
                           format(self.SITE_NAME, exc_type, exc_val, '\n'.join(traceback.format_tb(exc_tb))))
        return True
