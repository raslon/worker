import os

REDIS_HOST = 'redis://81.30.195.170'
REDIS_PASSWORD = 'ZigaKLghi()*&6bg(*&GBVy8ibphiB(_nipuh89'
EXCHANGE_NAME = 'worker#2'
LOG_LEVEL = 'INFO'
VERBOSE_MODE = 'True'
AUTO_START = 'True'
DB_HOST = '81.30.195.170'
DB_LOGIN = 'worker'
DB_PASSWORD = 'JHg7*&bhjjfd*(yh'
SENTRY_DSN = 'http://d29d7ec3a0f742c3b8789b7097f75249:6b19a46f4ad942508710254ebcb8a0b7@sentry.indexavto.com/3'
CAPTCHA_KEY = '59af9073253cb4212887d323780c0608'
LOGGING = {
    'version': 1,
    'disable_existing_loggers': True,
    'formatters': {
        'worker': {
            'format': '[%(asctime)s][%(levelname)s] %(message)s',
            'datefmt': '%H:%M:%S',
        },
    },

    'handlers': {
        'console': {
            'level': LOG_LEVEL,
            'class': 'logging.StreamHandler',
            'formatter': 'worker'
        },
        'sentry': {
            'level': LOG_LEVEL,
            'class': 'raven.handlers.logging.SentryHandler',
            'dsn': SENTRY_DSN,
        },
    },
    'loggers': {
        'aiow': {
            'handlers': ['console', 'sentry', ],
            'level': LOG_LEVEL,
            'propagate': False,
        },
    }
}
